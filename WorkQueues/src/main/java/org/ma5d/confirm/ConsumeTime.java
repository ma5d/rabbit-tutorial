package org.ma5d.confirm;

public class ConsumeTime {
    public static void main(String[] args) throws Exception {
        long a = System.currentTimeMillis();
        Individual.main(new String[0]);
        long b = System.currentTimeMillis();
        Batch.main(new String[0]);
        long c = System.currentTimeMillis();
        Async.main(new String[0]);
        long d = System.currentTimeMillis();
        System.out.println("Individual: " + (b - a));
        System.out.println("Batch: " + (c - b));
        System.out.println("Async: " + (d - c));
    }
}

// 发布5个单独确认消息,耗时19ms
// 10:44:04.901 [main] DEBUG com.rabbitmq.client.impl.ConsumerWorkService - Creating executor service with 12 thread(s) for consumer work service
//         发布5个批量确认消息,耗时8ms
// 10:44:04.943 [main] DEBUG com.rabbitmq.client.impl.ConsumerWorkService - Creating executor service with 12 thread(s) for consumer work service
//         发布5个异步确认消息,耗时1ms
// Individual: 301
// Batch: 49
// Async: 42