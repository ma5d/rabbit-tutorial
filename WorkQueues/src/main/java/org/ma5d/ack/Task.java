package org.ma5d.ack;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import org.ma5d.order.RabbitMqUtils;

import java.util.Scanner;

public class Task extends org.ma5d.order.Task {
    private static final String QUEUE_NAME = "ack_queue";
    private static Channel channel = RabbitMqUtils.getChannel();

    public static void main(String[] args) throws Exception {
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        // 从控制台当中接受信息
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String message = scanner.next();
            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
            System.out.println("发送消息完成:" + message);
        }
    }
}
