package org.ma5d.ack;

import com.rabbitmq.client.DeliverCallback;

public class Worker extends org.ma5d.order.Worker {
    protected static final String QUEUE_NAME = "ack_queue";

    DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        System.out.println(super.objectName + "接收到消息:" + new String(delivery.getBody()));
        super.channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
    };

    public static void main(String[] args) throws Exception {
        new Worker("worker01").runInstance();
        new Worker("worker02").runInstance();
    }

    @Override
    protected void runInstance() throws Exception {
        System.out.println(super.objectName + "消费者启动等待消费......");
        channel.basicConsume(QUEUE_NAME, false, deliverCallback, super.cancelCallback);
    }

    private Worker(String objectName) {
        super(objectName);
    }

}
