package org.ma5d.order;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

public class Worker {
    protected static final String QUEUE_NAME = "hello";
    protected Channel channel = RabbitMqUtils.getChannel();
    protected String objectName;
    protected DeliverCallback deliverCallback = (consumerTag, delivery) -> System.out.println(objectName + "接收到消息:" + new String(delivery.getBody()));
    protected CancelCallback cancelCallback = (consumerTag) -> System.out.println(Thread.currentThread().getName() + consumerTag + "消费者取消消费接口回调逻辑");

    public static void main(String[] args) throws Exception {
        new Worker("worker01").runInstance();
        new Worker("worker02").runInstance();
    }

    protected void runInstance() throws Exception {
        System.out.println(objectName + "消费者启动等待消费......");
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }

    protected Worker(String objectName) {
        this.objectName = objectName;
    }
}
