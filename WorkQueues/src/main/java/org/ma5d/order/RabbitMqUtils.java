package org.ma5d.order;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMqUtils {
    // 得到一个连接的 channel
    public static Channel getChannel() {
// 创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("ma-C92");
        factory.setUsername("admin");
        factory.setPassword("123");
        Channel channel;
        try {
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            // connection.close();
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return channel;
    }
}